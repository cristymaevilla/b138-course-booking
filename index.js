
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes= require("./routes/user");
const courseRoutes= require("./routes/course");

const app = express();


// connect to MONGODB:-----------------
mongoose.connect("mongodb+srv://dbcristyvilla:D4pe7npdr9J6r7ct@wdc028-course-booking.ktimy.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
);
mongoose.connection.once("open", ()=> console.log("Now connected to MongoDB Atlas"));
// -------------------------------------


app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines  the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);

// Defines  the "/courses" string to be included for all course routes defined in the "course" route file
app.use("/courses", courseRoutes);


// listening to port 
// process.env.PORT - environment variable ; or if the port is = to 4000 the console.log messg.will appear.
app.listen(process.env.PORT || 4000, ()=> {
	console.log(`API is now online on port ${process.env.PORT || 4000 }`)
});


