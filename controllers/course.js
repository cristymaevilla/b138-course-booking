const Course = require("../model/Course");
const auth= require("../auth")


// CREATE new course:
/*module.exports.addCourse=(reqBody)=> {
	// creates a var. "newCourse" & instantiates a new "Course" obj. using the mongoose model
	let newCourse = new Course
	({
	name: reqBody.name,
	description: reqBody.description,
	price: reqBody.price
	});
	// Save the created obj. to the database:
	return newCourse.save().then((course, error)=>{
		if (error) { return false;}
		else {return true;}
	})
}*/

// CREATE A COURSE if User is Admin------------
module.exports.addCourse = async (user, reqBody) => {
	if(user.isAdmin){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return (`You have no access`);
	}
}


// RETRIEVE ALL COURSES:----------------------
module.exports.getAllCourses = ()=> {
		return Course.find({}).then(result => {
			return result;
		})
}

// RETRIEVE ALL ACTIVE COURSES:----------------------
module.exports.getAllActive = ()=> {
		return Course.find({isActive : true}).then(result => {
			return result;
		})
}
// RETRIEVE A SPECIFIC COURSE:----------------------
module.exports.getCourse= (reqParams)=>{
	return Course.findById(reqParams.courseId).then(result =>{
		return result;
	})
}
// UPDATE A COURSE:---------------------------------
module.exports.updateCourse= async (user, reqParams, reqBody)=>{
	if(user.isAdmin){
		// Specify the fields of the doc. to be updated
		let updatedCourse = {
			name: reqBody.name,
			description:reqBody.description,
			price: reqBody.price
		}
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,error)=>{
			if(error){return false;}
			else{return true;}
		})
	}
	else{
		return (`You have no access`);
	}
}

// ARCHIVE A COURSE:---------------------------------
module.exports.archiveCourse= async (user, reqParams)=>{
	if(user.isAdmin){
		// let archiveCourse= {isActive :false}
		// reqParams.isActive=false;
		return Course.findByIdAndUpdate(reqParams.courseId, {isActive :false} ).then((course,error)=>{
			
			if(error){return false;}
			else{return true;}
		})
	}
	else{
		return (`You have no access`);
	}
}





