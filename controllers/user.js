const User = require("../model/User");
const Course=require("../model/Course")
// npm install bcrypt:
const bcrypt =require("bcrypt");
const auth= require("../auth")


// Check if the email already exists----------------
module.exports.checkEmailExists =(reqBody) =>{
	// .find method automatically compares the two and returns certain characters(result)
	return User.find({email: reqBody.email}).then(result=>{
		// 
		if (result.length > 0){
			return true;}
		else{return false;}
	})
}

// User Registration------------------------------------
module.exports.registerUser=(reqBody)=>{
	// "newUser" instanstiate new "User" object using mongoose model
	// useinfo from reqBody
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// to encrypt the password; 10-no.of salt rounds
		password:bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user,error)=>{
		if (error){return false;}
		else{ return true;}
	})
}

// User authentication--------------------------------------------
module.exports.loginUser=(reqBody)=>{
	// findOne- means first email that will match
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect= bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}
			else{ return false;}
		}
	})
}

// GET Profile controller:--------------------------------

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};




// =====================================================
// ANSWERS TO S33 ACTIVITY:

/*module.exports.getProfile =(reqBody) =>{
	return User.findById(reqBody.userId).then((result,error)=>{
		if (error){
			console.log(error);
				return false;}
		else{
			result.password="";
			return result;}
	})
}*/

// retrieve all users:-----------------------------
module.exports.retrieveAllUsers = ()=> {
		return User.find({}).then(result => {
			return result;
		})
}

// ENROLL USER to a course--------------------------

module.exports.enroll= async (userData, data)=>{
	if(userData.isAdmin){
		// add the courseId in the enrollment array of user:
		let isUserUpdated = await User.findById(userData.id).then(user => {
			// adds courseId in user's enrollment array
			user.enrollments.push({courseId : data.courseId});

			// saves the upadted user info. in detabase
			return user.save().then((user, error) => {
				if(error){return false;}
				else{return true;}
			})
		})

		// Add user in the enrollees array of the Course
		let isCourseUpdated = await Course.findById(data.courseId).then(course => {
			// Adds the userId in the course's enrollees array
			course.enrollees.push({userId: userData.id});

			// Save the updated course information in the database
			return course.save().then((course, error) => {
				if(error){return false;}
				else{return true;}
			})
		})

		// Condition that will check if the user and course docs. have been updated
		if(isUserUpdated && isCourseUpdated){return true;}
		else{return false;}
		
	}
	else{
		return (`You have no access`);
	}
}








/*module.exports.addCourse = async (user, reqBody) => {
	if(user.isAdmin){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return (`You have no access`);
	}
}*/