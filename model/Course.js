const mongoose = require("mongoose");

const courseSchema= new mongoose.Schema({
	name : {
		type:String,
		required : [true, "Course is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type:Number,
		required:[true, "Price is required"]
	},
	isActive:{
		type:Boolean,
		default:true
	},
	createdOn: {
		type:Date,
		default: new Date()
		// new Date -will instansiate/automate the date of creation(course)
	},
	enrollees: 
		[{
		userId:{
		type:String,
		required:[true, "UserId is required"]
			},
	enrolledOn: {
		type: Date,
		default: new Date()
			}

		}]
});


// will create a Course model based on courseSchema:
module.exports = mongoose.model("Course", courseSchema);
