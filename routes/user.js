const express = require("express");
const router = express.Router();

const userController= require("../controllers/user");

const auth= require("../auth")


// Route for checking if user's email already exists in the database
router.post("/checkEmail", (req,res)=> {
	userController.checkEmailExists(req.body).then(resultFromController=> res.send(resultFromController));
});

// Route for registering a user:
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=> res.send(resultFromController));
})

// route for authenticating a user:
router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(resultFromController=> res.send(resultFromController));
})


// ROUTE to retrieve user details with JWT--------------------------
// auth.verify method acts as a middleware to ensure that the user logged in b4 they can access specific app  features
router.get("/details", auth.verify,(req,res)=>{

	const userData= auth.decode(req.headers.authorization);
	// userData is the bearer token
	userController.getProfile({userId : userData.id}).then(resultFromController=> res.send(resultFromController));
})






// =================================================================
// ANSWERS TO MY ACTIVITY S33:
/*router.post("/details",(req,res)=>{
	userController.getProfile(req.body).then(resultFromController=> res.send(resultFromController));
})*/

// retrieve ALL user:----------------------------------
router.get("/", (req,res)=>{
	userController.retrieveAllUsers().then(resultFromController => res.send(
		resultFromController));
})
// ENROLL user to a course:----------------
/*router.post("/enroll", (req,res)=>{
	let data={
	userId: req.body.userId,
	courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(
		resultFromController));
})*/

// ACTIVITY S36 :ENROLL user to a course:----------------
router.post("/enroll", (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	console.log (userData)
	let data={
	courseId: req.body.courseId
	}
	userController.enroll(userData, data).then(resultFromController => res.send(
		resultFromController));
})
// allows us to export the "router" object that will be accessed in "index.js"
module.exports=router;





