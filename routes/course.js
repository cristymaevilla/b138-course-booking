const express = require("express");
const router = express.Router();

const courseController= require("../controllers/course");
const auth= require("../auth")

// Route for CREATING a course--------------
/*router.post("/", (req,res)=>{
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
})*/


//  ROUTE to CREATE a course if ADMIN-------------------
// auth.verify method acts as a middleware to ensure that the user logged in b4 they can access specific app  features

router.post("/", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization)

		courseController.addCourse(data, req.body).then(resultFromController => res.send(resultFromController));

		});



//route to retrieve ALL COURSES-----------------
router.get("/all", (req,res)=>{
	courseController.getAllCourses().then(resultFromController => res.send(
		resultFromController));
})

// Route for retrieveing ALL ACTIVE COURSES-----
router.get("/", (req,res)=>{
	courseController.getAllActive().then(resultFromController => res.send(
		resultFromController));
})

// Route for retrieveing a SPECIFIC COURSE-----
router.get("/:courseId", (req,res)=>{
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(resultFromController => res.send(
		resultFromController));
})

// Route for UPDATING a COURSE-----------------
router.put("/:courseId", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	courseController.updateCourse(user,req.params, req.body).then(resultFromController => res.send(
		resultFromController));
})
// Route for ARCHIVING  a COURSE-----------------
router.put("/:courseId/archive", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	courseController.archiveCourse(user,req.params).then(resultFromController => res.send(
		resultFromController));
})


module.exports=router